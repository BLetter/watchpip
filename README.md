# WatchPIP - Watcher para proyectos con Poetry y PyPiServer

Vigila carpetas *dist* para copiar archivos de distribucion *.whl* a un servidor local de **PyPiServer**

#### Formato de configuración

La configuración del watcher se basa en archivos en formato YAML (*.yml*).

```yaml
pypiserver:
  pathPackage: /home/user/pypiserver/packages
proyectos:
  - myProyect:
     distPath: /home/user/workbench/my_proyect/dist
  - other:
     distPath: /home/user/workbench/my_other_proyect/dist
```

- **pypiserver**: Info de configuracion para el pypiserver, principalmente *pathPackage* que indica la ruta absoluta del directorio donde se almacenan los paquetes de PyPiServer
- **proyectos**: Lista de proyectos a vigilar, se incluye la propiedad *distPath* donde está la ruta absoluta del directorio donde el proyecto genera los paquetes *.whl*.

#### Cómo lanzarlo

<code>
$ python watchpip.py --config /ruta/a/archivo/de/configuracion/yml
</code>

#### Daemonizar el proceso

Se puede utilizar **supervisor** para poder montar **WatchPIP** de forma similar a un servicio, aquí un ejemplo de archivo de configuración de supervisor

```
[program:watchpip]
command=/home/currentUser/virtualenv/watchpipEnv/bin/python watchpip.py --config ./config.yml 
user=currentUser
directory=/home/currentUser/workbench/watchpip
stdout_logfile=/var/log/watchpip.log
stderr_logfile=/var/log/watchpip_error.log
```


Hay que tener en cuenta la posbilidad de que el entorno virtual sobre el que estamos ejecutando **WatchPIP** no esté en el mismo directorio del código de
**WatchPIP** (p.e, estamos creando todos los entornos virtuales en un directorio centralizado, o estamos usando *pipenv* de forma normal); es por ello que el código de ejemplo distingue entre el entorno virtual y el directorio de **WatchPIP**.
