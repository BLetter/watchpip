# -*- coding: utf-8 -*-

from watchdog.observers import Observer

class PipObserver:

    @classmethod
    def createObserverFor(cls,proyecto,handler):

        path = proyecto.get('distPath','')

        observer = Observer()

        observer.schedule(handler,path,recursive=True)

        return observer