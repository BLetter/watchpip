# -*- coding: utf-8 -*-

import os
import logging

from typing import Dict

from watchdog.events import PatternMatchingEventHandler

class PoetryHandler:
    packagePath = ''

    @classmethod
    def setPackagePath(cls,path: str):
        cls.packagePath = path
    
    @staticmethod
    def on_created(event):
        logger=logging.getLogger(__name__)
        logger.setLevel(logging.INFO)
        
        logger.info(f'{event.src_path} has been created')
        baseName = os.path.basename(event.src_path)
        destFile = os.path.join(PoetryHandler.packagePath,baseName)
        os.system(f'cp {event.src_path} {destFile}')
        
    @staticmethod
    def buildHandlerFor(proyecto:Dict=None):
        patterns = '*.whl'
        ignore_patterns = ''
        ignore_directories = False
        case_sensitive = True

        event_handler = PatternMatchingEventHandler(patterns,ignore_patterns,ignore_directories,case_sensitive)

        event_handler.on_created = PoetryHandler.on_created

        return event_handler