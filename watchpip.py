# -*- coding: utf-8 -*-

import argparse
import time
import logging

from yaml import load,dump
try:
    from yaml import CLoader as Loader, CDumper as Dumper
except ImportError:
    from yaml import Loader,Dumper

from eventhandlers import PoetryHandler
from observers import PipObserver

logging.basicConfig()
logger=logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)

parser = argparse.ArgumentParser(description='Watchdog para PipServer')

parser.add_argument('--config',help='Ruta absoluta del archivo de configuracion en format YAML, por defecto busca config.yml en la ruta actual')

args=parser.parse_args()

configFile='./config.yml'

if args.config is not None:
    configFile=args.config

configuration=None
logger.info('Configuración en {}'.format(configFile))

try:
    with open(configFile,'r') as cFile:
        configuration=load(cFile,Loader=Loader)
except Exception as e:
    logger.error('Error al cargar la configuracion',exc_info=True)

#Crear handlers

PoetryHandler.setPackagePath(configuration.get('pypiserver',{}).get('pathPackage',''))

lista_observadores=list()
for p in configuration.get('proyectos',[]):
    for k in p.keys():
        logger.info('Watching {}'.format(k))
        handler=PoetryHandler.buildHandlerFor()
        observer=PipObserver.createObserverFor(p.get(k),handler)
        lista_observadores.append(observer)

for ob in lista_observadores:
    ob.start()

while True:
    time.sleep(5)

for ob in lista_observadores:
    ob.join()
